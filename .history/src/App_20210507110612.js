import React, { useEffect } from "react";
import { Redirect, Route, Switch, useHistory, useLocation } from "react-router";
import Contact from "./pages/Contact";
import Home from "./pages/Home";
import { Project1, Project2, Project3, Project4 } from "./pages/Projects";

const App = () => {
  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    const handleSrollToElement = (e) => {
      const url = window.location.origin + "/";
      const wheelRooter = (before, after) => {
        if (e.wheelDeltaY < 0) {
          history.push(after);
        } else if (e.wheelDeltaY > 0) {
          history.push(before);
        }
      };

      switch (window.location.href.toString()) {
        case url:
          if (e.wheelDeltaY < 0) {
            history.push("project-1");
          }
          break;
          case url + "project-1":
            wheelRooter('/', 'project-2')

        default:
          console.log("switch default");
      }
    };

    window.addEventListener("wheel", handleSrollToElement);
  }, [history]);
  return (
    <Switch location={location} key={location.pathname}>
      <Route exact path="/" component={Home} />
      <Route exact path="/project-1" component={Project1} />
      <Route exact path="/project-2" component={Project2} />
      <Route exact path="/project-3" component={Project3} />
      <Route exact path="/project-4" component={Project4} />
      <Route exact path="/contact" component={Contact} />
      <Redirect to="/" />
    </Switch>
  );
};

export default App;
