import React from "react";
import { Redirect, Route, Switch } from "react-router";
import Contact from "./pages/Contact";
import Home from "./pages/Home";

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/project-1" component={Project1} />
      <Route exact path="/project-2" component={Project1} />
      <Route exact path="/project-3" component={Project1} />
      <Route exact path="/project-4" component={Project1} />
      <Route exact path="/contact" component={Contact} />
      <Redirect to="/" />
    </Switch>
  );
};

export default App;
