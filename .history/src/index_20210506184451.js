import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";
import "./style/index.scss";
import{ init } from 'emailjs-com';
init("user_pRk3DGc43juejnGfKx81Q");

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById("root")
);
