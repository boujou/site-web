import React, { useState } from "react";
import { projectsData } from "../data/projectsData";

const Project = (props) => {
  const [currentProject] = useState(projectsData);
  const project = currentProject[props.projectNumber];

  return (
    <div>
      <h1>Projet numéro {props.projectNumber}</h1>
      {project.id}
    </div>
  );
};

export default Project;
