import React from 'react';

const GitlabIcon = () => {
    const anim = () => {
        let navLinks = document.querySelectorAll(".social-network a");
        navLinks.forEach((link) => {
          link.addEventListener("mouseover", (e) => {
            let attrX = e.offsetX - 20;
            let attrY = e.offsetY - 13;
    
            link.style.transform = `translate(${attrX}px, ${attrY}px)`;
          });
          link.addEventListener("mouseleave", () => {
            link.style.transform = "";
          });
        });
      };
    
    return (
        <div className='>
            Icon
        </div>
    );
};

export default GitlabIcon;