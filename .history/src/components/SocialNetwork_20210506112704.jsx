import React from "react";

const SocialNetwork = () => {
    const anim = () => {

    }

  return (
    <div className="social-network">
      <ul className="content">
        <a
          href="https://linkedin.com/in/jean-baptiste-josselin"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMoueh
        >
          <li>
            <i className="fab fa-linkedin-in"></i>
          </li>
        </a>
        <a
          href="https://gitlab.com/boujou"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMoueh
        >
          <li>
            <i className="fab fa-gitlab"></i>
          </li>
        </a>
      </ul>
    </div>
  );
};

export default SocialNetwork;
