import React, { useEffect } from "react";

const Dynamic = () => {

  useEffect(() => {

  },[])

  return (
    <span className="dynamic-text">
      <span className="simply">simple</span>
      <span className="text-target" id="target"></span>
    </span>
  );
};

export default Dynamic;
