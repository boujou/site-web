import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = () => {
    return (
        <div className="navigation">
            <ul>
                <NavLink to='/' exact className='hover' activeClassName='nav-active'>
                    <li>Accueil</li>
                </NavLink>
                    <li className="nav-portfolio">
                        <ul className="nav-project-1" >
                            <NavLink to='/project-1'activeClassName='nav-active' className='hover'>
                                <li>Projet 1</li>
                            </NavLink>
                        </ul>
                    </li>
            </ul>
        </div>
    );
};

export default Navbar;