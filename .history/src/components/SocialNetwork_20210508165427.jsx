import React from "react";
import { Anim } from './Services'

const SocialNetwork = () => {

  return (
    <div className="social-network">
      <ul className="content">
        <a
          href="https://linkedin.com/in/jean-baptiste-josselin"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-linkedin-in"></i>
          </li>
        </a>
        <a
          href="https://gitlab.com/boujou"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-gitlab"></i>
          </li>
        </a>
        <a
          href="./assets/file/CV-JB-JOSSELIN.pdf"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i class="fas fa-file-pdf"></i>{" "}
          </li>
        </a>
      </ul>
    </div>
  );
};

export default SocialNetwork;
