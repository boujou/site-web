import React from "react";
import { projectsData } from "../data/projectsData";
const [currentProject] = useState(projectsData);
  const project = currentProject[props.projectNumber];
  
const SocialNetwork = () => {
  const anim = () => {
    let navLinks = document.querySelectorAll(".social-network a");
    navLinks.forEach((link) => {
      link.addEventListener("mouseover", (e) => {
        let attrX = e.offsetX - 20;
        let attrY = e.offsetY - 13;

        link.style.transform = `translate(${attrX}px, ${attrY}px)`;
      });
      link.addEventListener("mouseleave", () => {
        link.style.transform = "";
      });
    });
  };

  return (
    <div className="social-network">
      <ul className="content">
        <a
          href="https://linkedin.com/in/jean-baptiste-josselin"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-linkedin-in"></i>
          </li>
        </a>
        <a
          href="https://gitlab.com/boujou"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-gitlab"></i>
          </li>
        </a>
        <a
          href="./assets/file/CV-JB-JOSSELIN.pdf"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i className="fas fa-file-pdf"></i>{" "}
          </li>
        </a>
      </ul>
      <a
            href={project.link}
            target="_blank"
            rel="noopener noreferrer"
            className="hover"
            onMouseOver={anim}
          >
            <i className="fab fa-gitlab"></i>
          </a>

    </div>
  );
};

export default SocialNetwork;
