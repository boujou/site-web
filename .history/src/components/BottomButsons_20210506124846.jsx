import React from 'react';
import { NavLink } from 'react-router-dom';

const BottomButsons = (props) => {
    return (
       <div className="scroll-bottom">
           <div className="sb-main">
        {props.left && (
            <NavLink to={props.left}
            className="left hover""
        )}
           </div>
       </div>
    );
};

export default BottomButsons;