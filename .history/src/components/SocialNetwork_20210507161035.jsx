import React from "react";
import { Link } from "react-router-dom";

const SocialNetwork = () => {
  const anim = () => {
    let navLinks = document.querySelectorAll(".social-network a");

    navLinks.forEach((link) => {
      link.addEventListener("mouseover", (e) => {
        let attrX = e.offsetX - 20;
        let attrY = e.offsetY - 13;

        link.style.transform = `translate(${attrX}px, ${attrY}px)`;
      });
      link.addEventListener("mouseleave", () => {
        link.style.transform = "";
      });
    });
  };

  return (
    <div className="social-network">
      <ul className="content">
        <a
          href="https://linkedin.com/in/jean-baptiste-josselin"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-linkedin-in"></i>
          </li>
        </a>
        <a
          href="https://gitlab.com/boujou"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-gitlab"></i>
          </li>
        </a>
        <Link
          to="/assets/file/CV_JB_JOSSELIN_bdg.pdf"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i class="fas fa-file-pdf"></i>{" "}
          </li>
        </Link>
      </ul>
    </div>
  );
};

export default SocialNetwork;
