import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = () => {
    return (
        <div className="navigation">
            <ul>
                <NavLink to='/' exact className='hover' activeClassName='nav-active'>
                    <li>Accueil</li>
                    li.nav-portfolio>ul.nav-project-1
                </NavLink>
            </ul>
        </div>
    );
};

export default Navbar;