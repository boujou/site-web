import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = () => {
    return (
        <div className="navigation">
            <ul>
                <NavLink to='/' exact className='hover' activeClassName='nav-active'>
                    <li>Accueil</li>
                    li.nav-portfolio>ul.
                </NavLink>
            </ul>
        </div>
    );
};

export default Navbar;