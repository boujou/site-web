import React from 'react';

const SocialNetwork = () => {
    return (
    <div className="social-network">
        <ul className="content"><a href="https://www.linkedin.com/in/jean-baptiste-josselin" target='blank' rel='noopener noreferrer' className='hover'>
            <li><i className="fab fa-linkedin-in"></i></li></a></ul>
    </div>
    );
};

export default SocialNetwork;