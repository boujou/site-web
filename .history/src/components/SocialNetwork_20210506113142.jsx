import React from "react";

const SocialNetwork = () => {
  const anim = () => {
let navLinks = document.querySelector('.social-network a')

navLinks.foreach(link => {
    link.addEventListener('mouseover', (e) => {
        let attrx = e.offsetX - 20;
        let attry = e.offsetY - 13
    })
})
};

  return (
    <div className="social-network">
      <ul className="content">
        <a
          href="https://linkedin.com/in/jean-baptiste-josselin"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-linkedin-in"></i>
          </li>
        </a>
        <a
          href="https://gitlab.com/boujou"
          target="blank"
          rel="noopener noreferrer"
          className="hover"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-gitlab"></i>
          </li>
        </a>
      </ul>
    </div>
  );
};

export default SocialNetwork;
