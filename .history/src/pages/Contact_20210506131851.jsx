import React from "react";
import BottomButtons from "../components/BottomButtons";

const Contact = () => {
  return (
    <div>
      <BottomButtons left={"/project-3"} right={"/contact"} />
    </div>
  );
};

export default Contact;
