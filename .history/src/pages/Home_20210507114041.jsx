import React from "react";
import BottomButtons from "../components/BottomButtons";
import Dynamic from "../components/Dynamic";
import Navbar from "../components/Navbar";
import SocialNetwork from "../components/SocialNetwork";
import { motion } from 'framer-motion'

const Home = () => {
  return (
    <div>
      <div className="home">
        <Navbar />
        <motion.div c
        <SocialNetwork />
        <div className="home-main">
          <div className="main-content">
            <h1>CRÉATION DE SITE WEB</h1>
            <h2>
              <Dynamic />
            </h2>
          </div>
        </div>
        <BottomButtons right={"/project-1"} />
      </div>
    </div>
  );
};

export default Home;
