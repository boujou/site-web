import React from "react";
import BottomButtons from "../components/BottomButtons";

const Contact = () => {
  return (
    <div>
      <BottomButtons left={"/project-4"} right={"/"} />
    </div>
  );
};

export default Contact;
