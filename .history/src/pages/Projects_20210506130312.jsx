import React from "react";
import BottomButsons from "../components/BottomButtons";
import Navbar from "../components/Navbar";

export const Project1 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        Projet 1
      </div>
      <BottomButsons left={'/'} right={'/project-2'}/>
    </main>
  );
};
export const Project2 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        Projet 2
      </div>
      <BottomButsons left={'/project-1'} right={'/project-3'}/>
    </main>
  );
};
export const Project3 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        Projet 3
      </div>
      <BottomButsons left={'/project-2'} right={'/project-'}/>
    </main>
  );
};
export const Project4 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        Projet 4
      </div>
      <BottomButsons />
    </main>
  );
};
