import React from 'react';
import Navbar from '../components/Navbar';
import SocialNetwork from '../components/SocialNetwork';

const Home = () => {
    return (
        <div>
            <Navbar />
            <SocialNetwork />
            .home-main
        </div>
    );
};

export default Home;