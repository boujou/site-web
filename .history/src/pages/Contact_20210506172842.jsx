import React from "react";
import CopyToClipboard from "react-copy-to-clipboard";
import BottomButtons from "../components/BottomButtons";
import Logo from "../components/Logo";
import Navbar from "../components/Navbar";

const Contact = () => {
  return (
    <main>
      <div className="contact">
        <Navbar />
        <Logo />
        {/* contact form */}
        <div className="contact-infos">
          <div className="address">
            <div className="content">
              <h4>adresse</h4>
              <p>17 avenue Bouisson Bertrand</p>
              <p>34090 MONTPELLIER</p>
            </div>
          </div>
        </div>
        <div className="phone">
          <div className="content">
            <h4>téléphone</h4>
            <CopyToClipboard text='0663959964' className='hover'>
              <p style={{cursor: 'pointer'}}className=></p>
            </CopyToClipboard>
          </div>
        </div>
      </div>
      <BottomButtons left={"/project-4"} right={"/"} />
    </main>
  );
};

export default Contact;
