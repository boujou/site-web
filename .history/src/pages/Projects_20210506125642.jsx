import React from "react";
import Navbar from "../components/Navbar";

export const Project1 = () => {
  return (
    <main>
      <div className="project">Projet 1</div>
      <Navbar
    </main>
  );
};
export const Project2 = () => {
  return (
    <main>
      <div className="project">Projet 2</div>
      <Navbar
    </main>
  );
};
export const Project3 = () => {
  return (
    <main>
      <div className="project">Projet 3</div>
      <Navbar
    </main>
  );
};
export const Project4 = () => {
  return (
    <main>
      <div className="project">Projet 4</div>
      <Navbar
    </main>
  );
};
