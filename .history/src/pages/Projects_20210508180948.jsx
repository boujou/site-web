import React from "react";
import BottomButtons from "../components/BottomButtons";
import Logo from "../components/Logo";
import Navbar from "../components/Navbar";
import Project from "../components/Project";

export const Project1 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        <Logo />
        <Project projectNumber={0} />
        <BottomButtons left={"/"} right={"/project-2"} />
      </div>
    </main>
  );i
};
export const Project2 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        <Logo />
        <Project projectNumber={1} />
        <BottomButtons left={"/project-1"} right={"/project-3"} />
      </div>
    </main>
  );
};
export const Project3 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        <Logo />
        <Project projectNumber={2} />
        <BottomButtons left={"/project-2"} right={"/project-4"} />
      </div>
    </main>
  );
};
export const Project4 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        <Logo />
        <Project projectNumber={3} />
        <BottomButtons left={"/project-3"} right={"/contact"} />
      </div>
    </main>
  );
};
