import React from "react";
import CopyToClipboard from "react-copy-to-clipboard";
import BottomButtons from "../components/BottomButtons";
import ContactForm from "../components/ContactForm";
import Logo from "../components/Logo";
import Navbar from "../components/Navbar";
import SocialNetwork from "../components/SocialNetwork";

const Contact = () => {
  return (
    <main>
      <div className="contact">
        <Navbar />
        <Logo />
        <ContactForm />
        <div className="contact-infos">
          <div className="address">
            <div className="content">
              <h4>adresse</h4>
              <p>17, Av Bouisson Bertrand</p>
              <p>34090 MONTPELLIER</p>
            </div>
          </div>
          <div className="phone">
            <div className="content">
              <h4>téléphone</h4>
              <CopyToClipboard text="0663959964" className="hover">
                <p
                  style={{ cursor: "pointer" }}
                  className="clipboard"
                  onClick={() => {
                    alert("Téléphone copié !");
                  }}
                >
                  06.63.95.99.64
                </p>
              </CopyToClipboard>
            </div>
          </div>
          <div className="email">
            <div className="content">
              <h4>email</h4>
              <CopyToClipboard
                text="jeanbaptistejosselin@gmail.com"
                className="hover"
              >
                <p
                  style={{ cursor: "pointer" }}
                  className="clipboard"
                  onClick={() => {
                    alert("Adresse mail copiée !");
                  }}
                >
                  jeanbaptistejosselin@gmail.com
                </p>
              </CopyToClipboard>
            </div>
          </div>
          <SocialNetwork />
          <div className="dredits">
            <p>Jean-Baptiste JOSSELIN - 2021</p>
          </div>
        </div>
        <BottomButtons
      </div>
    </main>
  );
};

export default Contact;
