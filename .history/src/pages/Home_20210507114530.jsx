import React from "react";
import BottomButtons from "../components/BottomButtons";
import Dynamic from "../components/Dynamic";
import Navbar from "../components/Navbar";
import SocialNetwork from "../components/SocialNetwork";
import { motion } from "framer-motion";

const Home = () => {
  const variants = {
    initial: {
      opacity: 0,
      transition: { duration: 0.5 },
      x: 100,
    },
    visible: {
      opacity: 1,
      x: 0,
    },
    exit: {
      transition: { duration: 0.3 },
      x: -100,
    },
  };

  return (
    <main>
      <Navbar />
      <motion.div 
      initial={initial}
    
      className="home">
        <SocialNetwork />
        <div className="home-main">
          <div className="main-content">
            <h1>CRÉATION DE SITE WEB</h1>
            <h2>
              <Dynamic />
            </h2>
          </div>
        </div>
        <BottomButtons right={"/project-1"} />
      </motion.div>
    </main>
  );
};

export default Home;
