import React from 'react';
import Dynamic from '../components/Dynamic';
import Navbar from '../components/Navbar';
import SocialNetwork from '../components/SocialNetwork';

const Home = () => {
    return (
        <div>
            <Navbar />
            <SocialNetwork />
            <div className="home-main">
                <div className="main-content">
                    <h1>CREATION DE SITE WEB</h1>
                    <h2>
                        <Dynamic 
                    </h2>
                </div>
            </div>
        </div>
    );
};

export default Home;