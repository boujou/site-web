import React from 'react';
import Navbar from '../components/Navbar';
import SocialNetwork from '../components/SocialNetwork';

const Home = () => {
    return (
        <div>
            <Navbar />
            <SocialNetwork />
            .home-main>.main-content>h1
        </div>
    );
};

export default Home;