import React from "react";
import BottomButtons from "../components/BottomButtons";
import Navbar from "../components/Navbar";

export const Project1 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        Projet 1
      </div>
      <BottomButtons left={'/'} right={'/project-2'} />
    </main>
  );
};
export const Project2 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        Projet 2
      </div>
      <BottomButtons left={'/project-1'} right={'/project-3'} />
    </main>
  );
};
export const Project3 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        Projet 3
      </div>
      <BottomButtons left={'/project-2'} right={'/project-4'} />
    </main>
  );
};
export const Project4 = () => {
  return (
    <main>
      <div className="project">
        <Navbar />
        Projet 4
      </div>
      <BottomButtons left={'/project-3'} right={'/contact'} />
    </main>
  );
};
