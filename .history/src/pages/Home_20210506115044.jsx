import React from 'react';
import Navbar from '../components/Navbar';
import SocialNetwork from '../components/SocialNetwork';

const Home = () => {
    return (
        <div>
            <Navbar />
            <SocialNetwork />
            <div className="home-main">
                <div className="main-content">
                    <h1>Développeur'</h1>
                </div>
            </div>
        </div>
    );
};

export default Home;