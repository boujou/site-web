import React from "react";
import BottomButtons from "../components/BottomButtons";
import Logo from "../components/Logo";
import Navbar from "../components/Navbar";

const Contact = () => {
  return (
    <main>
      <div className="contact">
        <Navbar />
        <Logo />
        {/* contact form */}
        <div className="contact-infos">
          <div className="address"></div>
        </div>
      </div>
      <BottomButtons left={"/project-4"} right={"/"} />
    </main>
  );
};

export default Contact;
