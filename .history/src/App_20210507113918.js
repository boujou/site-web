import React, { useEffect } from "react";
import { Redirect, Route, Switch, useHistory, useLocation } from "react-router";
import Contact from "./pages/Contact";
import Home from "./pages/Home";
import { Project1, Project2, Project3, Project4 } from "./pages/Projects";
import { AnimatePresence } from "framer-motion";

const App = () => {
  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    const handleSrollToElement = (e) => {
      const url = window.location.origin + "/";
      const wheelRooter = (before, after) => {
        if (e.wheelDeltaY < 0) {
          setTimeout(() => {
            history.push(after);
          }, 500);
        } else if (e.wheelDeltaY > 0) {
          setTimeout(() => {
            history.push(before);
          }, 500);
        }
      };

      switch (window.location.href.toString()) {
        case url:
          if (e.wheelDeltaY < 0) {
            setTimeout(() => {
              history.push("project-1");
            }, 500);
          }
          break;
        case url + "project-1":
          wheelRooter("", "project-2");
          break;
        case url + "project-2":
          wheelRooter("project-1", "project-3");
          break;
        case url + "project-3":
          wheelRooter("project-2", "project-4");
          break;
        case url + "project-4":
          wheelRooter("project-3", "contact");
          break;
        case url + "contact":
          if (e.wheelDeltaY > 0) {
            setTimeout(() => {
              history.push("project-4");
            }, 500);
          }
          break;

        default:
          console.log("switch default");
      }
    };

    window.addEventListener("wheel", handleSrollToElement);
  }, [history]);
  return (
    <AnimatePresence>≈
    <Switch location={location} key={location.pathname}>
      <Route exact path="/" component={Home} />
      <Route exact path="/project-1" component={Project1} />
      <Route exact path="/project-2" component={Project2} />
      <Route exact path="/project-3" component={Project3} />
      <Route exact path="/project-4" component={Project4} />
      <Route exact path="/contact" component={Contact} />
      <Redirect to="/" />
    </Switch>
    √
  );
};

export default App;
