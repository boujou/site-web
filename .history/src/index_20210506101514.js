import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Browser}
import '.style/index.scss';

ReactDOM.render(
    <App />,
  document.getElementById('root')
);
