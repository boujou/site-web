export const projectsData = [
  {
    id: 1,
    title: "My Social Network",
    date: "2021",
    languages: ["React", "Node", "Javascript", "Sass"],
    infos:
      "Application web de type résau social. Projet personnel réalisé durant une formation Fullstack Javascript",
    img: "./assets/img/my-social-network.png",
    link: "https://gitlab.com/boujou/projet-diginamic-client",
  },
  {
    id: 2,
    title: "Jeux RPG",
    date: "2021",
    languages: ["React", "Javascript"],
    infos:
      "Jeux RPG réalisé en Javascript avec Reactjs. Construction de décor  Projet personnel réalisé en autonomie",
    img: "./assets/img/projet-2.jpg",
    link: "https://gitlab.com/boujou/rpg-reactjs",
  },
  {
    id: 3,
    title: "Everpost",
    date: "Avril 2020",
    languages: ["Wordpress", "Php", "React"],
    infos:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quas cumque labore suscipit, pariatur laboriosam autem omnis saepe quisquam enim iste.",
    img: "./assets/img/projet-3.jpg",
    link: "",
  },
  {
    id: 4,
    title: "Creative Dev",
    date: "Juillet 2020",
    languages: ["Vue", "Php"],
    infos:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quas cumque labore suscipit, pariatur laboriosam autem omnis saepe quisquam enim iste.",
    img: "./assets/img/projet-4.jpg",
    link: "http://www.google.com",
  },
];
