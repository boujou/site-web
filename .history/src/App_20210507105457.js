import React, { useEffect } from "react";
import { Redirect, Route, Switch, useHistory, useLocation } from "react-router";
import Contact from "./pages/Contact";
import Home from "./pages/Home";
import { Project1, Project2, Project3, Project4 } from "./pages/Projects";

const App = () => {
  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    const handleSrollToElement = (e) => {
      const url = window.location.oriogin + "/";
      const wheelRooter = (before, after) => {
        if(e.whellDeltaY < 0) {
          history.location.href.to
        }
      }
    };

    window.addEventListener("wheel", handleSrollToElement);
  }, [history]);
  return (
    <Switch location={location} key={location.pathname}>
      <Route exact path="/" component={Home} />
      <Route exact path="/project-1" component={Project1} />
      <Route exact path="/project-2" component={Project2} />
      <Route exact path="/project-3" component={Project3} />
      <Route exact path="/project-4" component={Project4} />
      <Route exact path="/contact" component={Contact} />
      <Redirect to="/" />
    </Switch>
  );
};

export default App;
