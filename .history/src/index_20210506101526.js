import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter as Router } from 'react''
import '.style/index.scss';

ReactDOM.render(
    <App />,
  document.getElementById('root')
);
