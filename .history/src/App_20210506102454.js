import React from 'react';
import { Route, Switch } from 'react-router';
import Home from './pages/Home';

const App = () => {
  return (
    <Switch>
      <Route exact path='/' component={ Home }></Route>
    </Switch>
    );
};

export default App;